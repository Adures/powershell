<#
23.09.2020 version 2.5
Added maternity column
After finishing the script is now waiting for user input before closing
Changed filename, so it shows date when the report was created

20.07.2020 version 2.4
Fixed issue when not all users were included in report and got rid of "onmicrosoft" errors


-----------------------------------------------------------------------------------------------
The script checks for the MFA status in for users in Poland, Hungary, Sweden, Romania and Czech Republic. 
It returns the raport with MFA status of the users, their OU and information if they have Externall access group 


Adrian Stefaniak
#>






import-module MSOnline
import-module ActiveDirectory

#$CurrentUser = [string](Get-WMIObject -class Win32_ComputerSystem).username.Replace("EMEA\","") 
$global:Cred = Get-Credential -UserName $CurrentUser -Message "Login using adm account"
#$creds2 = Get-Credential -Message "Please provide your cloud adm credentials"

$path = "C:\Apps\adminemail.txt"
if (Test-Path $path) {
    $Adminemail = Get-Content $Path
    Connect-MsolService 
     Connect-ExchangeOnline -userPrincipalNAme $Adminemail -ShowProgress $True
    
} else {
  $Adminemail = Read-Host "Please type your eol admin email, you see this line only because you are using this script from a new device"
  $Adminemail | out-file "C:\Apps\adminemail.txt"
   
Connect-MsolService 
     Connect-ExchangeOnline -userPrincipalNAme $Adminemail -ShowProgress $True


}


function Write-Messages {
  [CmdletBinding()]
  param()
  Write-Host "`nHost message`n"
  Write-Output "`nOutput message`n"
  Write-Verbose "`nVerbose message`n"
  Write-Warning "`nWarning message`n"
  Write-Error "`nError message`n"
  Write-Debug "`nDebug message`n"
}



function cloudlastlogodate ($AccountUPN) {

 $lastcloudlogon = get-mailboxstatistics $AccountUPN | select lastuseractiontime
 $Global:lastcloudlogon = $lastcloudlogon.lastuseractionTime
Write-Verbose "Last logon date for $AccountUPN in the cloud is $lastcloudlogon"
}



function getgroups{
Write-Verbose "Loading Groups"
$Global:GroupPL = get-ADGroupMember "#GG-PL-O365-EXTERNALACCESS" | Select -ExpandProperty SamAccountName
Write-Verbose "#GG-PL-O365-EXTERNALACCESS loaded"
$Global:GroupCZ = get-ADGroupMember "#GG-CZ-O365-EXTERNALACCESS" | Select -ExpandProperty SamAccountName
Write-Verbose "#GG-CZ-O365-EXTERNALACCESS loaded"
$Global:GroupRO = get-ADGroupMember "#GG-RO-O365-EXTERNALACCESS" | Select -ExpandProperty SamAccountName
Write-Verbose "#GG-RO-O365-EXTERNALACCESS loaded"
$Global:GroupSE = get-ADGroupMember "#GG-SE-O365-EXTERNALACCESS" | Select -ExpandProperty SamAccountName
Write-Verbose "#GG-SE-O365-EXTERNALACCESS loaded"
$Global:GroupHU = get-ADGroupMember "#GG-HU-O365-EXTERNALACCESS" | Select -ExpandProperty SamAccountName
Write-Verbose "#GG-HU-O365-EXTERNALACCESS loaded"
$Global:GroupUK = get-ADGroupMember "#GG-UK-O365-EXTERNALACCESS" | Select -ExpandProperty SamAccountName
Write-Verbose "#GG-UK-O365-EXTERNALACCESS loaded"
}


function groupcheck ($AccountSAM) {
    $Zmienna = $False
     $Global:FoundGroup = "No Externall Access Group"
    write-verbose "jestem w group check i sprawdzam $AccountSAM"
     # $AccountAD =  Get-ADUser -Filter "userPrincipalName -like '$AccountUPN'"
      #$AccountSAM = $AccountAD.samaccountname
       

do {
    
        ForEach ($Member in $GroupPL) { 
     #   write-verbose "pętla For EACH PL"
        If ($Member -eq $AccountSAM) {
             $Global:FoundGroup = "#GG-PL-O365-EXTERNALACCESS"
             write-verbose "$AccountSAM znaleziono w PL"
             $Zmienna = $True
             break
        } Else {


       
       continue
        
        }
 
 }
 
     ForEach ($Member2 in $GroupCZ) { 
        If ($Member2 -eq $AccountSAM) {
             $Global:FoundGroup = "#GG-CZ-O365-EXTERNALACCESS"
             write-verbose " $AccountSAM znaleziono w CZ"
            $Zmienna = $True
             break
        } Else {


       continue
       
        
        }
 
 }
     ForEach ($Member3 in $GroupHU) { 
        If ($Member3 -eq $AccountSAM) {
             $Global:FoundGroup = "#GG-HU-O365-EXTERNALACCESS"
             write-verbose "$AccountSAM znaleziono w HU"
             $Zmienna = $True
             
        } Else {

        continue
       
       
        
        }
 
 }
    ForEach ($Member4 in $GroupSE) { 
        If ($Member4 -eq $AccountSAM) {
             $Global:FoundGroup = "#GG-SE-O365-EXTERNALACCESS"
             write-verbose "$AccountSAM znaleziono w SE"
             $Zmienna = $True
             
        } Else {

        continue
       
       
        
        }
 
 }
    ForEach ($Member5 in $GroupRO) { 
        If ($Member5 -eq $AccountSAM) {
             $Global:FoundGroup = "#GG-RO-O365-EXTERNALACCESS"
             write-verbose "$AccountSAM znaleziono w RO"
             $Zmienna = $True
             
        } Else {

        continue
       
       
        
        }
 
 }

 ForEach ($Member6 in $GroupUK) { 
        If ($Member6 -eq $AccountSAM) {
             $Global:FoundGroup = "#GG-UK-O365-EXTERNALACCESS"
             write-verbose "$AccountSAM znaleziono w UK"
             $Zmienna = $True
             
        } Else {
       
       break
       
       
        
        }
 
 }
  
    
 
 }
 
 Until ($Zmienna = $True)
 
 
 
 
 }



 
Function getOU ($AccountSAM) {
write-verbose "Sprawdzam OU $AccountSAM"

if ($AccountSAM -ne $null) {
$Global:Ou = Get-ADUser -identity $AccountSAM -Properties canonicalName | Select-Object @{Name='OU';Expression={$_.DistinguishedName.Split(',')[($_.Name -split ',').count..$($_.DistinguishedName.Split(',')).count] -join ','}}
   }

Else {

$Global:Ou = "not samaccountname, can't get OU"
 }
}

function useractivity ($AccountDate) {
$Zmienna = $false
$CurrentDate = Get-Date -Hour "08" -Minute "00" -Second "00"
$PreviousDate = $CurrentDate.adddays(-7)


$Global:UserActivity = $AccountDate
if ($AccountDate -gt $PreviousDate)

         { 
      $Global:UserActivity = $AccountDate
   write-verbose " I take user activity from AD, this is $accountDate = $useractivity, $Global:UserActivity"
   }

   Else {
  
  cloudlastlogodate ($AccountUPN)
  $Zmienna  = $true
 
  
   }



   if ($Zmienna -eq $True) {
 
  if ($lastcloudlogon -gt $AccountDate) {
  
  $Global:UserActivity =  $lastcloudlogon
 write-verbose "I take from the cloud $lastcloudlogon, $UserActivity, $Global:UserActivity"
  }
  else {

   $Global:UserActivity = $AccountDate
   write-verbose "after comparing, I take newer AD date $AccountDate, $UserActivity, $Global:UserActivity"
   }


   } else {
   $Global:UserActivity = $AccountDate
   
   }

}






 
Function mfacheck {

$Report = @()
$i = 0
$Accounts = (Get-MsolUser -All | where {$_.Country -Match 'Poland|Hungary|Czech Republic|Sweden|Romania'} | Sort DisplayName)
#$Accounts = (Get-MsolUser -userprincipalname stefaniak@hays.pl | Sort DisplayName)
Write-Verbose "Users loaded"

ForEach ($Account in $Accounts) {
  $AccountUPN = $Account.userprincipalname
  Write-Verbose "Processing $AccountUPN"

   $i++
   $Methods = $Account | Select -ExpandProperty StrongAuthenticationMethods
   $MFA = $Account | Select -ExpandProperty StrongAuthenticationUserDetails
   $State = $Account | Select -ExpandProperty StrongAuthenticationRequirements
   $Methods | ForEach { If ($_.IsDefault -eq $True) {$Method = $_.MethodType}}
   If ($State.State -ne $Null) {$MFAStatus = $State.State}
      Else {$MFAStatus = "Disabled"}
    
    If ($AccountUPN -notlike '*onmicrosoft*')
    {
      
      $AccountAD =  Get-ADUser -Properties * -Filter "userPrincipalName -like '$AccountUPN'"
      $AccountSAM = $AccountAD.samaccountname
      $AccountDate = $AccountAD.lastlogondate
      $AccountDescription = $AccountAD.Description
       Write-Verbose "Checking groups for $AccountSAM"
      groupcheck($AccountSAM)
       getOU($AccountSAM)
      
      Write-Verbose "Dla $AccountSAM grupa externall to $FoundGroup"
      } else {

      $AccountAD = "can't check on-premises"
      $AccountSAM = "can't check on-premises"
      $AccountDate = "can't check on-premises"
      $AccountDescription = "can't check on-premises"
      }

      if ($Ou -like '*Shared Mailboxes*') { 
      
      $Global:UserActivity = "Shared mailbox, no user activity"
      write-verbose "Shared mailbox, no user activity"
      } else {
      write-verbose "this is not shared mailbox"
      useractivity ($AccountDate)

      }
    
      #groupcheck2($AccountSAM)
      
     if ($AccountDescription -like '*aternity*') { 
      
      $Matternity = $AccountDescription
      
      } else {
        $Matternity = $null

      }


      $mydate = get-date -format 'dd_MM_yyyy'
      $reportname = 'FullReport_'
      $Global:filename = $reportname + $mydate



   $ReportLine = [PSCustomObject]@{
      UPN       = $Account.UserPrincipalName
       MFAMethod = $Method
       MFAEmail  = $MFA.Email
       MFAStatus = $MFAStatus 
       Country = $Account.Country 
       IsEnabledinAD = $AccountAD.enabled
      # LastLogonDateAD = $AccountDate
       #LactLogonDateCloud = $lastcloudlogon
       UserActivity = $UserActivity
       GroupMember = $FoundGroup
       OU = $Ou
       License = $Account.isLicensed
       MaternityStatus = $Matternity
       }
   $Report += $ReportLine      }

$Report | Export-CSV -NoTypeInformation C:\TempDownloads\$filename.CSV


}


function Main-Menu {
    param (
        [Parameter(Mandatory=$false)]
        [string]$Title = 'Menu'
    )
    changescreensize 30 60
    Clear-Host
    Start-Sleep -Milliseconds 200

    #add new menu entry, adjust switches + functions below menu section
Write-Host "================== $Title ==================="  -BackgroundColor DarkGreen
Write-Host "let's start"

$VerbosePreference = "continue"
getgroups
write-Host "Groups are loaded"
write-Host "Users loaded, proceeding with gathering information and generating raport. This might taka a while"
mfacheck
Write-Host "You can find the report in C:\TempDownloads\$filename.csv"
read-host "Press any key to close this window"
write-verbose "koniec"
}
main-menu
#Write-Messages -Verbose -Debug *> C:\TempDownloads\mfa_check_log.txt
