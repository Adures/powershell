$MFAExchangeModule = ((Get-ChildItem -Path $($env:LOCALAPPDATA+"\Apps\2.0\") -Filter CreateExoPSSession.ps1 -Recurse ).FullName | Select-Object -Last 1)
. "$MFAExchangeModule"

import-module ActiveDirectory



$global:Cred = Get-Credential -UserName $CurrentUser -Message "Login using adm account"
$Scriptdir = Split-Path $script:MyInvocation.MyCommand.Path


$path = "C:\Users\stefaniakad\Desktop\ITQuickTools v3\adminemail.txt"
if (Test-Path $path) {
    $Adminemail = Get-Content $Path
    Connect-EXOPSSession -UserPrincipalName $Adminemail
} else {
  $Adminemail = Read-Host "Please type your eol admin email"
  $Adminemail | out-file "C:\Users\stefaniakad\Desktop\ITQuickTools v3\adminemail.txt"
   Connect-EXOPSSession -UserPrincipalName $Adminemail


}











function Show-Menu
{
    param (
        [string]$Title = 'Exchange Online Quick Tools '
    )
    Clear-Host

    Write-Host "================== $Title ==================="  -BackgroundColor DarkGreen    
    Write-Host "1: Give Full Access permission to mailbox"
    Write-Host "2: Remove full access permission from mailbox"
    Write-Host "3: Forward mailbox "
    Write-Host "4: Remove Forward "
    Write-Host "5: Add vacation   "
    Write-Host "6: Remove vacation   "
    Write-Host "7: Quickly remove access "
   # Write-Host "8: leaver"
    Write-Host "9: Distgroup"
    Write-Host "Q: Press 'Q' to quit."
}

do
 {
     Show-Menu
     $selection = Read-Host "Please make a selection"
     switch ($selection)
     {
         '1' {
             
             $Usermailbox = Read-Host "Please type a mailbox that you be changing permissions"
             
             $Accessmailbox = Read-Host "Who do you want to give access to (type mailbox)"
Add-MailboxPermission -Identity $Accessmailbox -User $Usermailbox -AccessRight FullAccess
         } '2' {
              $Usermailbox = Read-Host "Please type a mailbox that you be changing permissions"
             
             $Accessmailbox = Read-Host "Who do you want to give access to (type mailbox)"
Remove-MailboxPermission -Identity $Accessmailbox -User $Usermailbox -AccessRight FullAccess
         } '3' {

         Write-Host "1: Forward and deliver"
         Write-Host "2: Only Forward"
          $selection2 = Read-Host "Please make a selection"
          switch($selection2){
          '1'{
            $Forwardedmailbox = Read-Host "Which mailbox do you want to forward"
              $Whereforwarded = Read-Host "Where do you want to forward it"
              Set-Mailbox -Identity "$Forwardedmailbox" -ForwardingSMTPAddress $Whereforwarded -DeliverToMailboxAndForward $true
              }

           '2'{
           $Forwardedmailbox = Read-Host "Which mailbox do you want to forward"
              $Whereforwarded = Read-Host "Where do you want to forward it"
              Set-Mailbox -Identity "$Forwardedmailbox" -ForwardingSMTPAddress $Whereforwarded -DeliverToMailboxAndForward $false
           }



              }

            }
         '4' {
              $Forwardedmailbox = Read-Host "From which mailbox do you want to remove forward"
              Set-Mailbox -Identity "$Forwardedmailbox" -ForwardingSMTPAddress $null -DeliverToMailboxAndForward $false
         }

         '5'{
         	
         #Add-Type -AssemblyName System.Windows.Forms
         #$FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ InitialDirectory = [Environment]::GetFolderPath('Desktop') }
         
         $Usermailbox = Read-Host "To which mailbox do you want to add vacation message"
         Write-Host "1: Choose a file with an vacation message"
         Write-Host "2: Create new Vacaton File"
             # switch($selection_vacation){
          #1{
          # Write-Host "2: Czy tutaj weszliśmy"
           #

#            }
 #          2{
  #         
     #      }
         
        # $Vacation = $FileBrowser.ShowDialog()
         
         #Set-MailboxAutoReplyConfiguration -Id $Usermailbox -AutoReplyState Enabled -InternalMessage "$Vacation" -ExternalMessage "$Vacation"
         
      #   }


            $selection_vacation = Read-Host "Please make a selection"
          switch($selection_vacation){
          '1'{
            # $Vacation = $FileBrowser.ShowDialog()
            $Vacationpath = $Scriptdir +'\'+ "vacation.txt"
             start-process Notepad.exe $Vacationpath -Wait
             $VacationMessage = Get-Content -Path $Vacationpath
             Set-MailboxAutoReplyConfiguration -Id $Usermailbox -AutoReplyState Enabled -InternalMessage "$Vacationmessage" -ExternalMessage "$Vacationmessage"
              }

           '2'{
          $Forwardedmailbox = Read-Host "Which mailbox do you want to forward"
             $Whereforwarded = Read-Host "Where do you want to forward it"
           Set-Mailbox -Identity "$Forwardedmailbox" -ForwardingSMTPAddress $Whereforwarded -DeliverToMailboxAndForward $false
           }



              }

            


            }
         '6'{
         $Usermailbox = Read-Host "To which mailbox do you want to remove vacation message"
         Set-MailboxAutoReplyConfiguration -Id $Usermailbox -AutoReplyState Disable -InternalMessage $null -ExternalMessage $null
         }

         '7'{
         $Email_which_will_be_deleted = Read-Host "Please provide a user's email address to remove all access"
         
      



         While ("y","n" -notcontains $answer) {
	$answer = Read-Host "Do you want to forward messages? ( y / n )?"
	}
If ($answer -eq 'y') {
	 $Email_where_to_forward = Read-Host "Please provide a user's email address where you want to forward messages"
Set-Mailbox -Identity "$Email_which_will_be_deleted" -ForwardingSMTPAddress $Email_where_to_forward 
Set-Mailbox $Email_which_will_be_deleted -Type shared
	}
Else
	{
	Remove-Variable * -ErrorAction SilentlyContinue
	}


Set-CASMailbox -Identity $Email_which_will_be_deleted  -ActiveSyncEnabled $false
Set-CASMailbox -Identity $Email_which_will_be_deleted -OWAEnabled $false


         }
         '8'{
         $Username = Read-Host "please provide username"
         $DistributionGroups= Get-DistributionGroup | where { (Get-DistributionGroupMember $_.Name | foreach {$_.PrimarySmtpAddress}) -contains "$Username"}
         }

         '9'{
         
$email = Read-Host "Please provide a user's email address to remove from all distribution groups"
$mailbox = Get-Mailbox -Identity $email
$DN=$mailbox.DistinguishedName
$Filter = "Members -like ""$DN"""
$DistributionGroupsList = Get-DistributionGroup -ResultSize Unlimited -Filter $Filter
Write-host `n
Write-host "Listing all Distribution Groups:"
Write-host `n
$DistributionGroupsList | ft
$answer = Read-Host "Would you like to proceed and remove $email from all distribution groups ( y / n )?" 
While ("y","n" -notcontains $answer) {
	$answer = Read-Host "Would you like to proceed and remove $email from all distribution groups ( y / n )?"
	}
If ($answer -eq 'y') {
	ForEach ($item in $DistributionGroupsList) {
		Remove-DistributionGroupMember -Identity $item.DisplayName –Member $email –BypassSecurityGroupManagerCheck -Confirm:$false
	}
	
	Write-host `n
	Write-host "Successfully removed"
	Remove-Variable * -ErrorAction SilentlyContinue
	}
Else
	{
	Remove-Variable * -ErrorAction SilentlyContinue
	}







         }
     }
     pause
 }
 until ($selection -eq 'q')



 
 
 
 #get-distributiongroup -ResultSize Unlimited | Where-Object {$_.primarysmtpaddress -like "*.pl" -or $_.primarysmtpaddress -like "*.com"} | Select-Object Name,Primarysmtpaddress