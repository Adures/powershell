Connect-MsolService -AzureEnvironment AzureCloud
connect-azuread 

$utente = Read-Host "Insert Username to enable 2FA"

$user = Get-ADUser  -Identity $utente | select userprincipalname
#Enable MFA
$mfa = New-Object -TypeName Microsoft.Online.Administration.StrongAuthenticationRequirement
$mfa.RelyingParty = "*"
$mfa.State = "Enabled"
#$mfa = @()
#Set-MsolUser -UserPrincipalName $user.userprincipalname -StrongAuthenticationRequirements $mfa
get-msoluser -UserPrincipalName $user.userprincipalname | where-object { $_.StrongAuthenticationRequirements.state -ne "enforced"} | set-MsolUser -UserPrincipalName $user.userprincipalname -StrongAuthenticationRequirements $mfa
#set-MsolUser -UserPrincipalName $user.userprincipalname -StrongAuthenticationRequirements $mfa
get-msoluser -UserPrincipalName $user.userprincipalname | select-object -ExpandProperty StrongAuthenticationRequirements
get-msoluser -UserPrincipalName $user.userprincipalname | select-object -ExpandProperty StrongAuthenticationMethods 
