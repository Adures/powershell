<#
11/06/2020
Script is used to remotely modify speed and duplex parameter on a device network adapter.
It checks and changes the value to "Auto Negotiation"
You can either choose one device or import csv file.
Keep in mind for the import to work the path must be without ""


Adrian Stefaniak
#>

#$global:Cred = Get-Credential -UserName $CurrentUser -Message "Login using adm account"
$VerbosePreference = "continue"





Function change_one_pc_settings ($PCNAME) {

write-verbose "Trying to connect"
if (Test-Connection -ComputerName $PCNAME -Count 1){
Write-verbose "Connected"

$adapter = Invoke-Command -ComputerName "$PCNAME" {Get-NetAdapterAdvancedProperty -Displayname "Speed & Duplex" | select name, displayname, displayvalue}
if ($adapter.displayvalue -eq "Auto Negotiation")
  {
 write-verbose "Adapter already had Auto Negotiation setting"
         } 
         else { 
         Invoke-Command -ComputerName "$PCNAME" {Set-NetAdapterAdvancedProperty -DisplayName ‘Speed & Duplex’ -DisplayValue ‘Auto Negotiation’}
                   write-verbose "Adapter settings were changed to Auto Negotiation"
                   
                   }



}

else{write-verbose "Device not available"}

}

Function change_many_pc_settings ($PCNAME) {
$CleanPC = $PCNAMe.computers
ForEach ($PC in $CleanPC) {

write-verbose "Trying to connect"
if (Test-Connection -ComputerName $PC -Count 1){
Write-verbose "Connected"

$adapter = Invoke-Command -ComputerName "$PC" {Get-NetAdapterAdvancedProperty -Displayname "Speed & Duplex" | select name, displayname, displayvalue}
if ($adapter.displayvalue -eq "Auto Negotiation")
  {
 write-verbose "Adapter already had Auto Negotiation setting"
       $adapter =  $adapter.displayvalue 
         } 
         else { 
         Invoke-Command -ComputerName "$PC" {Set-NetAdapterAdvancedProperty -DisplayName ‘Speed & Duplex’ -DisplayValue ‘Auto Negotiation’}
                   
         if($?)
          {
  write-verbose "Adapter settings were changed to Auto Negotiation"
   $adapter = "Auto Negotiation"
          }
else
       {
   write-verbose "Command to change adapter settings failed"
   $adapter = "Command to change adapter settings failed"
         }



                   }



}

else{write-verbose "Device not available"
    $adapter = "Could not connect to device"}

$ReportLine = [PSCustomObject]@{
     
       PC       = $PC
       Adapter = $adapter
    
      
       }
   $Report =  [Array]$Report  + $ReportLine      }

$Report | Export-CSV -NoTypeInformation C:\TempDownloads\PC_Report.CSV



}


function Show-Menu
{   
    param (
        [string]$Title = 'Exchange Online Quick Tools '
    )
    Clear-Host

    Write-Host "================== $Title ==================="  -BackgroundColor DarkGreen    
    Write-Host "1. Connect to one device"
    Write-Host "2. Load CSV file"
    
    Write-Host "Q: Press 'Q' to quit."
}

If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")){
$arguments = "& '" + $myinvocation.mycommand.definition + "'"
Start-Process powershell -Verb runAs -ArgumentList $arguments
Break
}
Else {



do
 {
     Show-Menu

    $selection = Read-Host "Please make a selection"
     switch ($selection)
     {
         '1' {
            $PCName = Read-Host "Insert PC name"
            change_one_pc_settings $PCName
         
         }
         '2'{
          
          $Path = Read-host "Please paste path to your csv file"
          $CSV = Import-CSV -path $Path
           change_many_pc_settings $CSV
         Write-host "You can find the file here:  C:\TempDownloads\PC_Report.CSV"
         Sleep 10

         }
     
   }
         
 pause
  }
   until ($selection -eq 'q')

   }